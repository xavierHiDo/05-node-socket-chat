const mongoose = require('mongoose');

const db_connection = async() => {
  try {
    await mongoose.connect(process.env.MONGODB_CNN, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false,
    });

    console.log('Conexion a base de datos exitoso');
  } catch (error) {
    console.log(error)
    throw new Error('Error al iniciar la base de datos');
  }
}

module.exports = {
  db_connection
}
