const path = require('path');
const fs = require('fs');
const cloudinary = require('cloudinary').v2;
cloudinary.config(process.env.CLOUDINARY_URL);
const { response } = require('express');
const { uploadFile } = require('../helpers');
const { User, Product} = require('../models');

const loadFile = async (req, res=response) => {
  try {
    // const temp_name = await uploadFile(req.files, ['txt'], 'textos');
    const temp_name = await uploadFile(req.files, undefined, 'images');

    res.status(201).json({
      temp_name
    });
  } catch (msg) {
    res.status(400).json({
      msg
    });
  }
}

const updateImage = async (req, res=response) => {
  const {collection, id} = req.params;

  let modelo;

  switch (collection) {
    case 'users':
      modelo = await User.findById(id);
      if(!modelo){
        return res.status(400).json({
          msg: 'No existe un usuario con ese ID'
        });
      }
      break;

    case 'products':
      modelo = await Product.findById(id);
      if(!modelo){
        return res.status(400).json({
          msg: 'No existe un producto con ese ID'
        });
      }
      break;

    default:
      return res.status(500).json({
        msg: 'Falto validar esta parte'
      });
  }

  if(modelo.img){
    const path_image = path.join(__dirname, '../uploads', collection, modelo.img);
    if(fs.existsSync(path_image)){
      fs.unlinkSync(path_image);
    }
  }

  const temp_name = await uploadFile(req.files, undefined, collection);
  modelo.img = temp_name;

  await modelo.save();

  res.json({
    modelo
  });
}

const updateImageCloudinary = async (req, res=response) => {
  const {collection, id} = req.params;

  let modelo;

  switch (collection) {
    case 'users':
      modelo = await User.findById(id);
      if(!modelo){
        return res.status(400).json({
          msg: 'No existe un usuario con ese ID'
        });
      }
      break;

    case 'products':
      modelo = await Product.findById(id);
      if(!modelo){
        return res.status(400).json({
          msg: 'No existe un producto con ese ID'
        });
      }
      break;

    default:
      return res.status(500).json({
        msg: 'Falto validar esta parte'
      });
  }

  if(modelo.img){
    const file_name = modelo.img.split('/').slice(-1)[0];
    const [public_id] = file_name.split('.');
    cloudinary.uploader.destroy(public_id);
  }

  const {tempFilePath} = req.files.file;
  const {secure_url} = await cloudinary.uploader.upload(tempFilePath);
  modelo.img = secure_url;

  await modelo.save();

  res.json({
    modelo
  });
}

const getImage = async (req, res=response) => {
  const {collection, id} = req.params;

  let modelo;

  switch (collection) {
    case 'users':
      modelo = await User.findById(id);
      if(!modelo){
        return res.status(400).json({
          msg: 'No existe un usuario con ese ID'
        });
      }
      break;

    case 'products':
      modelo = await Product.findById(id);
      if(!modelo){
        return res.status(400).json({
          msg: 'No existe un producto con ese ID'
        });
      }
      break;

    default:
      return res.status(500).json({
        msg: 'Falto validar esta parte'
      });
  }

  if(modelo.img){
    const path_image = path.join(__dirname, '../uploads', collection, modelo.img);
    if(fs.existsSync(path_image)){
      return res.sendFile(path_image);
    }
  }

  const path_image = path.join(__dirname, '../assets/no-image.jpg');
  return res.sendFile(path_image);
}

module.exports = {
  loadFile,
  updateImage,
  updateImageCloudinary,
  getImage
}