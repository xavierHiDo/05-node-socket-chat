const form = document.querySelector('form');
const url = (window.location.hostname.includes('localhost'))
  ? 'http://localhost:8080/api/auth/'
  : 'https://xavier-node-restserver.herokuapp.com/api/auth/';

form.addEventListener('submit', event => {
  event.preventDefault();
  const form_data = {};
  for(let element of form.elements){
    if(element.name.length > 0){
      form_data[element.name] = element.value;
    }
  }
  console.log('form_data', form_data);

  fetch(url + 'login', {
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(form_data)
  })
  .then(res => res.json())
  .then(({msg, token}) => {
    console.log('Respuesta del servidor', token);
    if (token){
      localStorage.setItem('token', token);
      window.location = 'chat.html';
    }
  })
  .catch(console.log);
});

function onSignIn(googleUser) {
  var profile = googleUser.getBasicProfile();
  console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
  console.log('Name: ' + profile.getName());
  console.log('Image URL: ' + profile.getImageUrl());
  console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.

  var id_token = googleUser.getAuthResponse().id_token;
  console.log(id_token);

  fetch(url + 'google-login', {
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify({id_token})
  })
  .then(res => res.json())
  .then(({token}) => {
    console.log('Respuesta del servidor', token);
    if (token){
      localStorage.setItem('token', token);
      window.location = 'chat.html';
    }
  })
  .catch(console.log);

}

function signOut() {
  var auth2 = gapi.auth2.getAuthInstance();
  auth2.signOut().then(function () {
    console.log('User signed out.');
  });
}