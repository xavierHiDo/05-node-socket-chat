console.log('Chat');
const url = (window.location.hostname.includes('localhost'))
  ? 'http://localhost:8080/api/auth/'
  : 'https://xavier-node-restserver.herokuapp.com/api/auth/';

let user = null;
let socket = null;
const txt_uid = document.querySelector('#txt_uid');
const txt_message = document.querySelector('#txt_message');
const ul_users = document.querySelector('#ul_users');
const ul_messages = document.querySelector('#ul_messages');
const btn_logout = document.querySelector('#btn_logout');

const validateJWT = async () => {
  const token = localStorage.getItem('token') || '';
  if(token.length <= 10) {
    window.location = 'index.html';
    throw new Error('No hay token');
  }

  const resp = await fetch(url, {
    headers: {'Authorization': token}
  });

  const {user: userDB, token: tokenDB} = await resp.json();
  console.log(userDB, tokenDB);
  localStorage.setItem('token', tokenDB);
  user = userDB;
  document.title = user.name
  await connectSocket();
}

const connectSocket = () => {
  socket = io({
    'extraHeaders': {
      'Authorization': localStorage.getItem('token')
    }
  });

  socket.on('connect', () => {
    console.log('Socket online');
  });

  socket.on('disconnect', () => {
    console.log('Socket offline');
  });

  socket.on('receive-messages', showmMessages);

  socket.on('active-users', showUsers);

  socket.on('private-message', (payload) => {
    console.log('Mensaje privado: ', payload);
  });
}

const showUsers = (users = []) => {
  let userHtml = '';
  users.forEach(({name, uid}) => {
    userHtml += `
      <li>
        <p>
          <h5 class="text-success">${name}</h5>
          <span class="fs-6 text-muted">${uid}</span>
        </p>
      </li>
    `;
  });
  ul_users.innerHTML = userHtml;
}

const showmMessages = (messages = []) => {
  let messageHtml = '';
  messages.forEach(({name, message}) => {
    messageHtml += `
      <li>
        <p>
          <h5 class="text-primary">${name}</h5>
          <span>${message}</span>
        </p>
      </li>
    `;
  });
  ul_messages.innerHTML = messageHtml;
}

txt_message.addEventListener('keyup', ({keyCode}) => {
  const message = txt_message.value;
  const uid = txt_uid.value;
  if(keyCode !== 13){
    return;
  }
  if(message.length === 0){
    return;
  }
  socket.emit('send-message', {uid, message});
  txt_message.value = '';
});

const main = async () => {
  await validateJWT();
}

main();