const { checkJWT } = require("../helpers");
const { ChatMessages } = require("../models");

const chatMessages = new ChatMessages();

const socketController = async (socket, io) => {
    console.log('Cliente conectado', socket.id);

    const user = await checkJWT(socket.handshake.headers['authorization']);
    if(!user){
      return socket.disconnect();
    }
    console.log('Se conecto', user.name);

    chatMessages.connectUser(user);
    io.emit('active-users', chatMessages.usersArr); //Mandarle a todos los usuarios conectados
    socket.emit('receive-messages', chatMessages.last10); //Mandarle solo al usuario que se conecta

    //Sala especial para poder mandar mensajes privados
    socket.join(user.id); //global, socket.id, user.id

    socket.on('disconnect', () => {
      console.log('Cliente desconectado', socket.id);
      chatMessages.disconnectUser(user.id);
      io.emit('active-users', chatMessages.usersArr);
    });

    socket.on('send-message', ({uid, message}) => {
      if(uid){
        //Mensaje privado
        socket.to(uid).emit('private-message', {from: user.name, message});
      }else{
        chatMessages.sendMessage(user.id, user.name, message);
        io.emit('receive-messages', chatMessages.last10);
      }
    });
}

module.exports = {
    socketController
}