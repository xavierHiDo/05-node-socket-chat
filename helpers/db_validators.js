const Role = require('../models/role.model');
const User = require('../models/user.model');
const Category = require('../models/category.model');
const { Product } = require('../models');

const validate_role = async (role = '') => {
  const is_role_exists = await Role.findOne({role});
  if (!is_role_exists) {
    throw new Error(`El rol ${role} no esta registrado en la DB`);
  }
}

const validate_email_exists = async (email = '') => {
  const is_email_exists = await User.findOne({email});
  if (is_email_exists) {
    throw new Error(`El correo ${email} ya esta registrado`);
  }
}

const validate_user_id_exists = async (user_id = '') => {
  const is_user_exists = await User.findById(user_id);
  if (!is_user_exists) {
    throw new Error(`El ID ${user_id} no existe`);
  }
}

const validate_category_id_exists = async (category_id = '') => {
  const is_category_exists = await Category.findById(category_id);
  if (!is_category_exists) {
    throw new Error(`El ID ${category_id} no existe`);
  }
}

const validate_product_id_exists = async (product_id = '') => {
  const is_product_exists = await Product.findById(product_id);
  if (!is_product_exists) {
    throw new Error(`El ID ${product_id} no existe`);
  }
}

const validate_collections = (collection='', allowed_collections=[]) => {
  const is_collection_exists = allowed_collections.includes(collection);
  if (!is_collection_exists) {
    throw new Error(`La colección ${collection} no es permitida - ${allowed_collections}`);
  }
  return true;
}

module.exports = {
  validate_role,
  validate_email_exists,
  validate_user_id_exists,
  validate_category_id_exists,
  validate_product_id_exists,
  validate_collections
}