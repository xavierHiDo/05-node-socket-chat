const validate_fields = require('./validate_fields');
const validate_jwt = require('./validate_jwt');
const validate_roles = require('./validate_roles');
const validateFile = require('./validate-file');

module.exports = {
  ...validate_fields,
  ...validate_jwt,
  ...validate_roles,
  ...validateFile
}