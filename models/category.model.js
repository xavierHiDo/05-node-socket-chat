const { Schema, model } = require('mongoose');

let category_schema = Schema({
  name: {
    type: String,
    required: [true, 'Nombre obligatorio'],
    unique: true
  },
  state: {
    type: Boolean,
    default: true,
    required: true
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  }
});

category_schema.methods.toJSON = function() {
  const { __v, state, ...category } = this.toObject();
  return category;
}

module.exports = model('Category', category_schema);