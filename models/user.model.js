const { Schema, model } = require('mongoose');

let user_schema = Schema({
  name: {
    type: String,
    required: [true, 'Nombre obligatorio']
  },
  email: {
    type: String,
    required: [true, 'Correo obligatorio'],
    unique: true
  },
  password: {
    type: String,
    required: [true, 'Contraseña obligatorio']
  },
  img: {
    type: String,
    required: false
  },
  role: {
    type: String,
    default: 'USER_ROLE',
    required: true
  },
  state: {
    type: Boolean,
    default: true
  },
  google: {
    type: Boolean,
    default: false
  },
});

user_schema.methods.toJSON = function() {
  const { __v, password, _id, ...user } = this.toObject();
  user.uid = _id
  return user;
}

module.exports = model('User', user_schema);