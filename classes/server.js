const express = require('express')
const cors = require('cors')
const fileUpload = require('express-fileupload');
const { db_connection } = require('../database/config');
const { createServer } = require('http');
const { socketController } = require('../sockets/socket.controller');

class Server {
  constructor() {
    this.app = express();
    this.port = process.env.PORT;
    this.server = createServer(this.app);
    this.io = require('socket.io')(this.server);
    this.paths = {
      auth: '/api/auth',
      users: '/api/users',
      categories: '/api/categories',
      products: '/api/products',
      search: '/api/search',
      upload: '/api/upload',
    };

    //Connect to database
      this.connect_db();

    //Middlewares
    this.middlewares();

    //Routes
    this.routes();

    //Sockets
    this.sockets()
  }

  connect_db() {
    db_connection();
  }

  middlewares() {
    this.app.use(cors());
    this.app.use(express.json()); //Body reading and parsing
    this.app.use( express.static('public'));
    this.app.use(fileUpload({
      useTempFiles : true,
      tempFileDir : '/tmp/',
      createParentPath: true
    }));
  }

  routes() {
    this.app.use(this.paths.auth, require('../routes/auth.route'));
    this.app.use(this.paths.users, require('../routes/user.route'));
    this.app.use(this.paths.categories, require('../routes/category.route'));
    this.app.use(this.paths.products, require('../routes/product.route'));
    this.app.use(this.paths.search, require('../routes/search.route'));
    this.app.use(this.paths.upload, require('../routes/upload.route'));
  }

  sockets() {
    this.io.on('connection', (socket) => socketController(socket, this.io));
  }

  start() {
    this.server.listen(this.port, () => {
      console.log('Server running on port: ', this.port);
    });
  }
}

module.exports = Server;